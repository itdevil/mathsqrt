package com.hcmus.math;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.prefs.BackingStoreException;

public class MathSqrt {
	
	
	public BigDecimal findS0(BigDecimal w)
	{
//		can S = cana *^2n
		int length = w.toPlainString().length();
		
		int n = 0;
		if (length > 2)
		{
			if (length % 2 != 0){
				n = (length  - 1)/2;
				int _2n = length - 1;
				
				BigDecimal s0 = w.divide(new BigDecimal(Math.pow(10, _2n)));
				
				if (s0.compareTo(new BigDecimal(10)) < 0){
					
					return new BigDecimal(2).multiply(new BigDecimal(Math.pow(10,n)));
					
				}else{
					return new BigDecimal(6).multiply(new BigDecimal(Math.pow(10,n)));
				}
					
			}else{
				
				n = (length - 2 ) /2;
				int _2n = length - 2;
				
				BigDecimal s0 = w.divide(new BigDecimal(Math.pow(10, _2n)));
				
				if (s0.compareTo(new BigDecimal(10)) < 0){
					
					return new BigDecimal(2).multiply(new BigDecimal(Math.pow(10,n)));
					
				}else{
					return new BigDecimal(6).multiply(new BigDecimal(Math.pow(10,n)));
				}
				
			}
		}
		
		if (w.compareTo(new BigDecimal(10)) < 0){
			
			return new BigDecimal(2).multiply(new BigDecimal(Math.pow(10,n)));
			
		}else{
			return new BigDecimal(6).multiply(new BigDecimal(Math.pow(10,n)));
		}
		
		
	}
	
	private boolean validate_Number(){
		
		return true;
	}
	
	public BigDecimal mysqrt(BigDecimal w,int accuracy) {
		
		BigDecimal s0 = this.findS0(w);
		
		
		BigDecimal s1 = new BigDecimal(0);
		BigDecimal contant = new BigDecimal(0.5);
		
		int count  = 0;
		System.out.println("s0 : " + s0 );
		while (true){
			
			BigDecimal t1 = w.divide(s0,MathContext.DECIMAL128);
			BigDecimal t2 = s0.add(t1);
			
			s1 = contant.multiply(t2);
			System.out.println("X : [" + count++ + "]:"+s1);
			
			
			if (s1.subtract(s0).abs(MathContext.DECIMAL128).compareTo(s0.multiply(new BigDecimal(1).divide(new BigDecimal(10).pow(accuracy),MathContext.DECIMAL128))) < 0){
				break;
			}
			
			
			s0 = s1;
			
			if (count > 500)
				break;
		}
		
		return s1;

	}
}
