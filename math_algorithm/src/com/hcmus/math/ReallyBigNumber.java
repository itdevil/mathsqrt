package com.hcmus.math;

import java.util.Arrays;
import java.util.LinkedList;

public class ReallyBigNumber {

	private LinkedList<Character> phan_nguyen;
	private LinkedList<Character> phan_thap_phan;
	private Character sign;// dau cua so
	private Character dot;// dau cham dong

	public ReallyBigNumber(long number) {
		// TODO Auto-generated constructor stub

		phan_nguyen = new LinkedList<Character>();
		phan_thap_phan = new LinkedList<Character>();
		sign = null;
		dot = null;

		if (number < 0) {

			String n = String.valueOf(number);
			char[] chars = n.toCharArray();
			int length = chars.length;

			sign = new Character('-');
			for (int i = 1; i < length; i++) {
				phan_nguyen.add(new Character(chars[i]));
			}

		} else {
			String n = String.valueOf(number);
			char[] chars = n.toCharArray();
			int length = chars.length;

			for (int i = 0; i < length; i++) {
				phan_nguyen.add(new Character(chars[i]));
			}
		}

	}

	public String toString() {

		StringBuilder builder = new StringBuilder();
		if (sign != null && sign.equals('-')) {
			builder.append('-');
		}
		
		for (Character t : this.phan_nguyen) {

			builder.append(t);

		}

		if (dot != null && dot.equals('.')) {
			builder.append('.');

		}

		for (Character t : this.phan_thap_phan) {

			builder.append(t);

		}

		return builder.toString();

	}

	public ReallyBigNumber(String n) {
		
		phan_nguyen = new LinkedList<Character>();
		phan_thap_phan = new LinkedList<Character>();
		sign = null;
		dot = null;
		if (n.startsWith("-")) {


			char sign_s = n.charAt(0);

			String nguyen = n.substring(1, n.indexOf('.'));
			String thap_phan = n.substring(n.indexOf('.') + 1, n.length());

			System.out.println(nguyen);

			System.out.println(thap_phan);

			sign = new Character(sign_s);

			while (nguyen.startsWith("0")){
				nguyen = nguyen.replaceFirst("0", "");
			}
			
			if (nguyen.equals("")){
				nguyen = "0";
			}
			
			
			char[] chars_nguyen = nguyen.toCharArray();
			int length_nguyen = chars_nguyen.length;
			
			for (int i = 0; i < length_nguyen; i++) {
				phan_nguyen.add(new Character(chars_nguyen[i]));
			}
			
			System.out.println(phan_nguyen);

			dot = new Character('.');

			char[] chars_thap_phan = thap_phan.toCharArray();
			int length_thap_phan = chars_thap_phan.length;

			for (int i = 0; i < length_thap_phan; i++) {
				phan_thap_phan.add(new Character(chars_thap_phan[i]));
			}
			

			System.out.println(phan_thap_phan);

		} else {

			String nguyen = n.substring(0, n.indexOf('.'));
			String thap_phan = n.substring(n.indexOf('.') + 1, n.length());

			while (nguyen.startsWith("0")){
				nguyen = nguyen.replaceFirst("0", "");
			}
			
			if (nguyen.equals("")){
				nguyen = "0";
			}

			System.out.println(thap_phan);

			char[] chars_nguyen = nguyen.toCharArray();
			int length_nguyen = chars_nguyen.length;

			for (int i = 0; i < length_nguyen; i++) {
				phan_nguyen.add(new Character(chars_nguyen[i]));
			}

			System.out.println(phan_nguyen);

			dot = new Character('.');

			char[] chars_thap_phan = thap_phan.toCharArray();
			int length_thap_phan = chars_thap_phan.length;

			for (int i = 0; i < length_thap_phan; i++) {
				phan_thap_phan.add(new Character(chars_thap_phan[i]));
			}

			System.out.println(phan_thap_phan);

		}
		
		
		// TODO Auto-generated constructor stub
	}

	public ReallyBigNumber(double number) {

		phan_nguyen = new LinkedList<Character>();
		phan_thap_phan = new LinkedList<Character>();
		sign = null;
		dot = null;

		if (number < 0) {

			String n = String.valueOf(number);

			char sign_s = n.charAt(0);

			String nguyen = n.substring(1, n.indexOf('.'));
			String thap_phan = n.substring(n.indexOf('.') + 1, n.length());

			System.out.println(nguyen);

			System.out.println(thap_phan);

			sign = new Character(sign_s);

			char[] chars_nguyen = nguyen.toCharArray();
			int length_nguyen = chars_nguyen.length;

			for (int i = 0; i < length_nguyen; i++) {
				phan_nguyen.add(new Character(chars_nguyen[i]));
			}
			
			System.out.println(phan_nguyen);

			dot = new Character('.');

			char[] chars_thap_phan = thap_phan.toCharArray();
			int length_thap_phan = chars_thap_phan.length;

			for (int i = 0; i < length_thap_phan; i++) {
				phan_thap_phan.add(new Character(chars_thap_phan[i]));
			}
			

			System.out.println(phan_thap_phan);

		} else {
			String n = String.valueOf(number);

			String nguyen = n.substring(0, n.indexOf('.'));
			String thap_phan = n.substring(n.indexOf('.') + 1, n.length());

			System.out.println(nguyen);

			System.out.println(thap_phan);

			char[] chars_nguyen = nguyen.toCharArray();
			int length_nguyen = chars_nguyen.length;

			for (int i = 0; i < length_nguyen; i++) {
				phan_nguyen.add(new Character(chars_nguyen[i]));
			}

			System.out.println(phan_nguyen);

			dot = new Character('.');

			char[] chars_thap_phan = thap_phan.toCharArray();
			int length_thap_phan = chars_thap_phan.length;

			for (int i = 0; i < length_thap_phan; i++) {
				phan_thap_phan.add(new Character(chars_thap_phan[i]));
			}

			System.out.println(phan_thap_phan);

		}

	}

	
	public boolean isNegative(){
		if (this.sign != null && this.sign .equals("-")){
			return true;
		}
		
		return false;
	}
	
	public int compare(ReallyBigNumber number){
		if (this.isNegative() && number.isNegative()){
			
			if( this.getPhan_nguyen().size() == number.getPhan_nguyen().size()){
				
				int count = 0;
				do {
					count ++;
					 if(this.getPhan_nguyen().get(count).compareTo(number.getPhan_nguyen().get(count)) == 0){
						 continue;
					 }else if(this.getPhan_nguyen().get(count).compareTo(number.getPhan_nguyen().get(count)) < 0){
						 // this > number
						 return 1;
					 }else if(this.getPhan_nguyen().get(count).compareTo(number.getPhan_nguyen().get(count)) > 0){
						 // this < number
						 return -1;
					 }
					 
				}while(count < this.getPhan_nguyen().size());
				
				if (this.getPhan_thap_phan().isEmpty() && number.getPhan_thap_phan().isEmpty()){
					// this == number
					return 0;
				}else if (this.getPhan_thap_phan().isEmpty() && !number.getPhan_thap_phan().isEmpty()){
					// this > number -1 > -1.2
					
					return 1;
					
				}else if (!this.getPhan_thap_phan().isEmpty() && number.getPhan_thap_phan().isEmpty()){
					// this > number -1.2 < -1
					
					return -1;
					
				}else if(!this.getPhan_thap_phan().isEmpty() && !number.getPhan_thap_phan().isEmpty()){
				    count = 0;
				    
				    int limit = 0;
				    
				    if (this.getPhan_thap_phan().size() == number.getPhan_thap_phan().size()){
				    	limit = this.getPhan_thap_phan().size();
				    }else if(this.getPhan_thap_phan().size() > number.getPhan_thap_phan().size()){
				    	limit = number.getPhan_thap_phan().size();
				    }else if(this.getPhan_thap_phan().size() < number.getPhan_thap_phan().size()){
				    	limit = this.getPhan_thap_phan().size();
				    }
				     
					do {
						count ++;
						 if(this.getPhan_thap_phan().get(count).compareTo(number.getPhan_thap_phan().get(count)) == 0){
							 continue;
						 }else if(this.getPhan_nguyen().get(count).compareTo(number.getPhan_nguyen().get(count)) < 0){
							 // this > number -1.23 > -1.33
							 return 1;
						 }else if(this.getPhan_nguyen().get(count).compareTo(number.getPhan_nguyen().get(count)) > 0){
							 // this < number - 1.33 < -1.23 
							 return -1;
						 }
						 
					}while(count < limit);
					
					
					if (this.getPhan_thap_phan().size() == number.getPhan_thap_phan().size()){
				    	// this == number
						return 0;
				    }else if(this.getPhan_thap_phan().size() > number.getPhan_thap_phan().size()){
				    	limit = number.getPhan_thap_phan().size();
				    }else if(this.getPhan_thap_phan().size() < number.getPhan_thap_phan().size()){
				    	limit = this.getPhan_thap_phan().size();
				    }
					
				}
				
				
				if(this.getPhan_thap_phan().size() == number.getPhan_thap_phan().size()){
					
				}
				
			}else if(this.getPhan_nguyen().size() > number.getPhan_nguyen().size()){
				
			}else if (this.getPhan_nguyen().size() < number.getPhan_nguyen().size()){
				
			}
			
		}else if(!this.isNegative() && number.isNegative()){
			return 1;
		}else if(this.isNegative() && !number.isNegative()){
			return -1;
		}else if(!this.isNegative() && !number.isNegative()){
			
		}
		return dot;
	}
	
	public ReallyBigNumber add(ReallyBigNumber number) {
		
		

		return null;

	}

	public ReallyBigNumber multiply() {
		return null;

	}

	public ReallyBigNumber divide() {
		return null;

	}

	public ReallyBigNumber subtract() {
		return null;

	}

	public LinkedList<Character> getPhan_nguyen() {
		return phan_nguyen;
	}

	public void setPhan_nguyen(LinkedList<Character> phan_nguyen) {
		this.phan_nguyen = phan_nguyen;
	}

	public LinkedList<Character> getPhan_thap_phan() {
		return phan_thap_phan;
	}

	public void setPhan_thap_phan(LinkedList<Character> phan_thap_phan) {
		this.phan_thap_phan = phan_thap_phan;
	}

	public Character getSign() {
		return sign;
	}

	public void setSign(Character sign) {
		this.sign = sign;
	}

	public Character getDot() {
		return dot;
	}

	public void setDot(Character dot) {
		this.dot = dot;
	}
	
	

}
