package com.hcmus.math;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class math_square_root {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			int accuracy = 34;
			BigDecimal number = null;
			if (args != null && args.length > 0) {
				for (int i = 0; i < args.length; i++) {

					if ("-acc".equals(args[i])) {
						accuracy = Integer.parseInt(args[i + 1]);
					} else if ("-n".equals(args[i])) {
						number = new BigDecimal(args[i + 1]);

					}

				}
			}
			
			System.out.println("number : " + number);
			System.out.println("accuracy : " + accuracy);
			
			MathSqrt test = new MathSqrt();

			StringBuilder builder = new StringBuilder();
			for (int j = 0; j < accuracy; j++) {
				builder.append("0");
			}

			DecimalFormat df = new DecimalFormat("#,###." + builder.toString());
			
			BigDecimal result = test.mysqrt(number, accuracy);
			System.out.println(
					"result square root of  " + number.toPlainString() + " : " + df.format(result));
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}

	}

}
