#!/usr/bin/python

import sys, getopt
from decimal import Decimal,getcontext

class MySqrt:
    def __init__(self,number,accuracy,context):
        self.epsilon = Decimal(1).__truediv__(Decimal(10).__pow__(accuracy))
        self.number = Decimal(number)
        self.context = context
        self.accuracy = accuracy


    def find_X0(self):

        # is count of digits of the number
        #  example : number is 2 , -> length = 1
        #  number is 123, -> length is 3
        # number will be present as number = pow(a,2) * pow(10,2n)
        # -> sqrt(number) = sqrt(a) * pow(10,n)
        # pow(a,2) < 10 -> a = 2
        #  pow(a,2) > 10 -> a = 6
        length = len(self.number.to_eng_string())
        n = 0
        if length > 2:
            if length % 2 != 0:
                n = (length - 1) / 2
                _2n = length - 1

                self.context.prec = 2*n + self.accuracy + 3

                p = self.number.__truediv__(Decimal(pow(10,_2n)),context=self.context)

                if p.compare(Decimal(10)) < 0:
                    return Decimal(2).__mul__(Decimal(pow(10,n)),context=self.context)
                else:
                    return Decimal(6).__mul__(Decimal(pow(10, n)),context=self.context)

            else:
                n = (length - 2) / 2
                _2n = length - 2

                self.context.prec = 2*n + self.accuracy

                p = self.number.__truediv__(Decimal(pow(10, _2n)),context=self.context)
                if p.compare(Decimal(10)) < 0:
                    return Decimal(2).__mul__(Decimal(pow(10, n)),context=self.context)
                else:
                    return Decimal(6).__mul__(Decimal(pow(10, n)),context=self.context)
        else:

            self.context.prec = 2*n + self.accuracy + 3

            if self.number.compare(Decimal(10)) < 0:
                return Decimal(2).__mul__(Decimal(pow(10, n),context=self.context))
            else:
                return Decimal(6).__mul__(Decimal(pow(10, n)),context=self.context)

    def mysqrt(self):
        x_0 = self.find_X0()

        x_1 = Decimal(0)
        contant = Decimal(0.5)

        print "X_0 : %s " % x_0.to_eng_string()

        count = 0
        while(True):
            count += 1
            temp_1 = self.number.__truediv__(x_0,context=self.context)
            temp_2 = x_0.__add__(temp_1)

            x_1 = contant.__mul__(temp_2,context=self.context)
            print "X : [%s ] : %s " % (count,x_1.to_eng_string())

            if x_1.__sub__(x_0, context=self.context).__abs__(context=self.context).compare(self.epsilon) < 0:
                break;

            x_0 = Decimal(x_1)

            if count > 1000:
                print "Avoiding loop forever"
                break;

        return x_1

def main(argv):

    number = ''
    precision = ''
    try:
        opts, args = getopt.getopt(argv, "hn:p:", ["number=", "precision="])
    except getopt.GetoptError:
        print 'mysqrt.py -n <number which need to get square root> -p <precision, number of digit after decimal point>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'mysqrt.py -n <number which need to get square root> -p <precision, number of digit after decimal point>'
            sys.exit()
        elif opt in ("-n", "--number"):
            number = arg
        elif opt in ("-p", "--precision"):
            precision = arg

    if number == '':
        print 'mysqrt.py -n <number which need to get square root> -p <precision, number of digit after decimal point>'
    elif precision == '':
        print 'mysqrt.py -n <number which need to get square root> -p <precision, number of digit after decimal point>'

    try:
        number_valid = Decimal(number)

        if number_valid.is_signed():
            print "%s is signed. Could not get square root " %  number_valid
        elif number_valid.is_zero():
            print "%s is zero. Could not get square root " % number_valid
        else:
            pre_valid = int(precision)
            mynumber = MySqrt(number_valid,pre_valid,getcontext())
            print "*************** Begin Processing ***************************"
            result = mynumber.mysqrt()
            print "*************** End Processing ***************************"

            print "    "

            index = result.to_eng_string().find(".", 0, len(result.to_eng_string()))
            decimal =  result.to_eng_string()[index + 1: index + 1 + pre_valid]
            interal = result.to_eng_string()[0: index]

            print "***************INPUT***************************"
            print "Number : %s " % number_valid
            print "Precision : %s " % precision
            print "Epsilon : %s" % mynumber.accuracy
            print "***************INPUT***************************"

            print "    "
            print "****************OUTPUT***************************"
            print "RESULT OF SQUARE ROOT : %s.%s " % (interal,decimal)
            print "****************OUTPUT***************************"

            print "    "

            print "****************OUTPUT***************************"
            print "Decimal part ( length = %s ) : %s " % (len(decimal), decimal)
            print "Interal part ( length = %s ) : %s " % (len(interal), interal)
            print "****************OUTPUT***************************"

    except Exception,e:
        print e;



if __name__ == "__main__":
   main(sys.argv[1:])